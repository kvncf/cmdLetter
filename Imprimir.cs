﻿using System;

namespace cmdLetter
{
    class Imprimir
    {
        private int vel = 100;
        private bool mensajeEnter = true;

        public Imprimir()
        {
        }

        public Imprimir(int vel, bool mensajeEnter)
        {
            setVel(vel);
            setMensajeEnter(mensajeEnter);
        }

        public void setVel(int vel)
        {
            this.vel = vel;
        }

        public void setMensajeEnter(bool mensajeEnter)
        {
            this.mensajeEnter = mensajeEnter;
        }

        public void imprimir(string texto)
        {
            for (int i = 0; i < texto.Length; i++)
            {
                Console.Write(texto[i]);
                System.Threading.Thread.Sleep(vel);
            }

            if (this.mensajeEnter)
            {
                wait();
            }
        }

        public void wait()
        {
            Console.Write("\n\n\t\t --- PRESIONA ENTER PARA CONTINUAR... ---\n\n\n\n");
            Console.ReadLine();
        }
    }
}
