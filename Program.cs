﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cmdLetter
{
    class Program
    {
        static void Main(string[] args)
        {

            Console.Title = "Titulo de carta...!";
            Imprimir i = new Imprimir(10, false);
            i.imprimir("imprimiendo carta..");
            //Cambiamos tiempo o vel
            i.setVel(100);
            i.imprimir("\n\ncontinuamos escribiendo...");
            //colocamos una espera para que el usuario continúe
            i.wait();

            i.setVel(5);
            i.imprimir("        00000000000           000000000000        \n      00000000     00000   000000     0000000      \n    0000000             000              00000     \n   0000000               0                 0000    \n  000000         FELIZ CUMPLEAÑOS           0000   \n  00000                                      0000  \n 00000                                      00000  \n 00000                                     000000  \n  000000                                 0000000   \n   0000000                              0000000    \n     000000                            000000      \n       000000                        000000        \n          00000                     0000           \n             0000                 0000             \n               0000             000                \n                 000         000                   \n                    000     00                     \n                      00  00                       ");
            i.imprimir("\n\n\nSaludos..!! \n\nkvn CF");
            i.wait();
        }
    }
}
